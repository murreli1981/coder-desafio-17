const db = require("../db/db");

module.exports = class MensajeDAO {
  async createMensaje({ email, date, message }) {
    await db("mensajes").insert({ email, date, message });
  }

  async readMensajes() {
    return db("mensajes").select("email", "date", "message");
  }

  async deleteMensaje() {
    await db("mensajes").del();
  }

  async updateMensaje(id, payload) {
    await db("mensajes").where({ id }).update(payload);
  }
};
