exports.up = (knex) =>
  knex.schema.createTable("mensajes", (table) => {
    table.increments("id").primary();
    table.string("email", 30).notNullable();
    table.string("date", 50).notNullable();
    table.string("message", 200).notNullable();
  });

exports.down = (knex) => knex.schema.dropTable("mensajes");
