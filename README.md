# coder-desafio-17

DB en los mensajes de websockets

## Create migrations

npx knex migrate:make init --migrations-directory src/modulos/db/migrations

## Update migrations

npx knex migrate:latest --knexfile src/modulos/db/knexfile.js

## Rollback last batch of migrations

npx knex migrate:down --knexfile src/modulos/db/knexfile.js

## endpoints:

### UI Chat

http://localhost:8080/api/ingreso

### Delete all messages

[EDIT] localhost:8080/api/mensajes

### Update message by id

[PUT] localhost:8080/api/mensajes/:id
body
{ message : "mensaje editado"}

### Scripts SQL

src/resources/scripts
